<?php

  namespace SiteManagement\Plugins;

  class Posttype
  {
    const POST_TYPE = 'plugins';

    public function __construct()
    {
      add_action('init', [$this, 'registerPosttype']);
    }

    /* ---
      Functions
    --- */

    public function registerPosttype()
    {
      register_post_type(self::POST_TYPE, [
          'labels'         => [
            'name' => 'Plugins',
          ],
          'public'        => false,
          'show_ui'       => true,
          'menu_position' => 10,
          'menu_icon'     => 'dashicons-chart-area',
          'supports'      => ['title', 'revisions'],
      ]);
    }
  }