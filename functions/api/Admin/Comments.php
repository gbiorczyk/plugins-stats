<?php

  namespace SiteManagement\Admin;

  class Comments
  {
    public function __construct()
    {
      add_filter('admin_menu', [$this, 'removeComments']);
    }

    /* ---
      Functions
    --- */

    public function removeComments()
    {
      remove_menu_page('edit-comments.php');
    }
  }