<?php

  namespace SiteManagement\Admin;

  class Posttypes
  {
    public function __construct()
    {
      add_filter('register_post_type_args', [$this, 'removePosttypes']);
    }

    /* ---
      Functions
    --- */

    public function removePosttypes($args)
    {
      if (!isset($args['_builtin']) || !$args['_builtin']) {
        return $args;
      }
      $args['public'] = false;
      return $args;
    }
  }