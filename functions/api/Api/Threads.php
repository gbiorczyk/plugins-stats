<?php

  namespace SiteManagement\Api;

  use SiteManagement\Api\StatsAbstract;
  use SiteManagement\Cache\Database;

  class Threads extends StatsAbstract
  {
    /* ---
      Functions
    --- */

    public function getStatsTable()
    {
      return Database::TABLE_NAME_THREADS;
    }

    public function parseResponse($stats, $params, $plugin)
    {
      $items = [];
      foreach ($stats as $index => $stat) {
        $dataCurrent = json_decode($stat->value, true);
        $dataPrev    = ($index > 0) ? json_decode($stats[$index -1]->value, true) : [];

        $items[] = [
          'date'         => $stat->date,
          'new_threads'  => ($dataCurrent['support_threads'] - ($dataPrev['support_threads'] ?? 0)),
          'new_resolved' => ($dataCurrent['support_threads_resolved'] - ($dataPrev['support_threads_resolved'] ?? 0)),
          'version'      => $this->getLatestPluginVersion($plugin->id, $stat->date),
        ];
      }

      return $items;
    }
  }