<?php

  namespace SiteManagement\Api;

  class Endpoints
  {
    const REST_NAMESPACE = 'api/v1';

    private $endpoints = [
      'growth'   => __NAMESPACE__ . '\Growth',
      'installs' => __NAMESPACE__ . '\Installs',
      'ratings'  => __NAMESPACE__ . '\Ratings',
      'threads'  => __NAMESPACE__ . '\Threads',
    ];

    public function __construct()
    {
      add_action('rest_api_init', [$this, 'registerEndpoints']);
    }

    /* ---
      Functions
    --- */

    public function registerEndpoints()
    {
      foreach ($this->endpoints as $route => $className) {
        register_rest_route(
          self::REST_NAMESPACE,
          $route,
          [
            'methods'  => 'GET',
            'callback' => [(new $className), 'loadResponse'],
            'args'     => [
              'slug' => [
                'description'       => 'Slug plugin',
                'required'          => true,
                'validate_callback' => function() {
                  return true;
                },
              ],
            ],
          ]
        );
      }
    }
  }