<?php

  namespace SiteManagement\Api;

  use SiteManagement\Api\StatsAbstract;
  use SiteManagement\Cache\Database;

  class Growth extends StatsAbstract
  {
    /* ---
      Functions
    --- */

    public function getStatsTable()
    {
      return Database::TABLE_NAME_STATS;
    }

    public function parseResponse($stats, $params, $plugin)
    {
      $installsStep   = $this->getLatestInstallsStep($plugin);
      $growthIndex    = $this->findClosestGrowthIndex($stats, $installsStep);
      $growthInstalls = $this->calculcateStepInstalls($installsStep, $stats[$growthIndex]);

      $items = [];
      foreach ($stats as $index => $stat) {
        $items[] = [
          'date'         => $stat->date,
          'value'        => number_format($stat->value, 1, '.', ''),
          'installs'     => ($index === $growthIndex) ? $growthInstalls : 0,
          'new_installs' => 0,
          'version'      => $this->getLatestPluginVersion($plugin->id, $stat->date, true),
        ];
      }
      $items = $this->calculateInstallsForSteps($items, $growthIndex);
      $items = $this->calculateNewInstallsForSteps($items);

      return $items;
    }

    private function getLatestInstallsStep($plugin)
    {
      global $wpdb;
      $table  = $wpdb->prefix . Database::TABLE_NAME_INSTALLS;
      $result = $wpdb->get_row("SELECT value, date FROM ${table} WHERE plugin_id = '{$plugin->id}' ORDER BY date DESC");
      if (!$result) return null;
      $result->date = date('Y-m-d', strtotime('-7 days', strtotime($result->date)));
      return $result;
    }

    private function findClosestGrowthIndex($stats, $installsStep)
    {
      $intervals = [];
      foreach ($stats as $index => $stat) {
        $intervals[] = [
          'interval' => abs(strtotime($stat->date) - strtotime($installsStep->date)),
          'index'    => $index,
        ];
      }
      asort($intervals);
      return reset($intervals)['index'];
    }

    private function calculcateStepInstalls($installsStep, $growthStep)
    {
      $days  = ((strtotime($growthStep->date) - strtotime($installsStep->date)) / (24 * 60 * 60));
      $value = (($installsStep->value * ($growthStep->value / 100)) / 7);
      return ($installsStep->value + ($days * $value));
    }

    private function calculateInstallsForSteps($stats, $growthIndex)
    {
      for ($i = ($growthIndex - 1); $i >= 0; $i--) {
        $stats[$i]['installs'] = ($stats[$i + 1]['installs'] / (1 + ($stats[$i + 1]['value'] / 100)));
      }
      for ($i = ($growthIndex + 1); $i < count($stats); $i++) {
        $stats[$i]['installs'] = ($stats[$i - 1]['installs'] * (1 + ($stats[$i]['value'] / 100)));
      }
      return $stats;
    }

    private function calculateNewInstallsForSteps($stats)
    {
      foreach ($stats as $index => $stat) {
        if ($index > 0) {
          $stat['new_installs'] = ($stat['installs'] - $stats[$index - 1]['installs']);
        }

        $stats[$index]['installs']     = round($stat['installs']);
        $stats[$index]['new_installs'] = round($stat['new_installs']);
      }
      return $stats;
    }
  }