<?php

  namespace SiteManagement\Api;

  use SiteManagement\Api\StatsAbstract;
  use SiteManagement\Cache\Database;

  class Ratings extends StatsAbstract
  {
    /* ---
      Functions
    --- */

    public function getStatsTable()
    {
      return Database::TABLE_NAME_RATINGS;
    }

    public function parseResponse($stats, $params, $plugin)
    {
      $items = [];
      foreach ($stats as $index => $stat) {
        $dataCurrent = json_decode($stat->value, true);
        $dataPrev    = ($index > 0) ? json_decode($stats[$index -1]->value, true) : [];

        $items[] = [
          'date'    => $stat->date,
          'stars_1' => ($dataCurrent['1'] - ($dataPrev['1'] ?? 0)),
          'stars_2' => ($dataCurrent['2'] - ($dataPrev['2'] ?? 0)),
          'stars_3' => ($dataCurrent['3'] - ($dataPrev['3'] ?? 0)),
          'stars_4' => ($dataCurrent['4'] - ($dataPrev['4'] ?? 0)),
          'stars_5' => ($dataCurrent['5'] - ($dataPrev['5'] ?? 0)),
          'rating'  => $this->getCurrentRaing($dataCurrent),
          'version' => $this->getLatestPluginVersion($plugin->id, $stat->date),
        ];
      }

      return $items;
    }

    private function getCurrentRaing($data)
    {
      $sum   = 0;
      $count = 0;
      foreach ($data as $stars => $amount) {
        $sum   += ($amount * $stars);
        $count += $amount;
      }
      return number_format(($sum / $count), 2, '.', '');
    }
  }