<?php

  namespace SiteManagement\Api;

  use SiteManagement\Api\StatsAbstract;
  use SiteManagement\Cache\Database;

  class Installs extends StatsAbstract
  {
    /* ---
      Functions
    --- */

    public function getStatsTable()
    {
      return Database::TABLE_NAME_INSTALLS;
    }

    public function parseResponse($stats, $params, $plugin)
    {
      $items = [];
      foreach ($stats as $index => $stat) {
        if ($stat->value % $plugin->step !== 0) continue;

        $items[] = [
          'date'    => $stat->date,
          'value'   => $stat->value,
          'days'    => (!$items) ? 0 : $this->getDaysBetweenDates($stat->date, end($items)['date']),
          'version' => $this->getLatestPluginVersion($plugin->id, $stat->date),
        ];
      }

      return $items;
    }

    private function getDaysBetweenDates($dateCurrent, $datePrev)
    {
      $dateStart = strtotime($datePrev);
      $dateEnd   = strtotime($dateCurrent);
      return ceil(abs($dateEnd - $dateStart) / (24 * 60 * 60));
    }
  }