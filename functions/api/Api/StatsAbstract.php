<?php

  namespace SiteManagement\Api;

  use SiteManagement\Cache\Database;

  abstract class StatsAbstract
  {
    /* ---
      Functions
    --- */

    public function loadResponse($request)
    {
      $params   = $request->get_params();
      $plugin   = $this->getPluginData($params['slug']);
      $items    = ($plugin) ? $this->loadStatsFromCache($plugin->id) : [];
      $response = $this->parseResponse($items, $params, $plugin);

      return new \WP_REST_Response([
        'resultCount' => count($response),
        'results'     => $response,
      ], 200);
    }

    protected function getPluginData($pluginSlug)
    {
      global $wpdb;
      $table  = $wpdb->prefix . Database::TABLE_NAME_PLUGINS;
      $plugin = $wpdb->get_row("SELECT * FROM {$table} WHERE slug = '{$pluginSlug}'");
      return $plugin ?: null;
    }

    protected function loadStatsFromCache($pluginId)
    {
      global $wpdb;
      $table   = $wpdb->prefix . $this->getStatsTable();
      $results = $wpdb->get_results("SELECT * FROM {$table} WHERE plugin_id = '{$pluginId}' ORDER BY date ASC");
      return $results ?: [];
    }

    protected function getLatestPluginVersion($pluginId, $date, $isPrevWeek = false)
    {
      global $wpdb;
      $table   = $wpdb->prefix . Database::TABLE_NAME_VERSIONS;
      $maxDate = ($isPrevWeek === true) ? date('Y-m-d', strtotime('+7 days', strtotime($date))) : $date;
      $result  = $wpdb->get_row("SELECT value, date FROM ${table} WHERE plugin_id = '{$pluginId}' AND date <= '{$maxDate}' ORDER BY date DESC");
      return $result->value ?? null;
    }
  }