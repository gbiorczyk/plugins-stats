<?php

  namespace SiteManagement;

  class SiteManagement
  {
    public function __construct()
    {
      new Admin\_Core();
      new Api\_Core();
      new Cache\_Core();
      new Plugins\_Core();
    }
  }