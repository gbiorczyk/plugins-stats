<?php

  namespace SiteManagement\Cache;

  use SiteManagement\Cache\Database;

  class Stats
  {
    const API_URL_STATS = 'https://api.wordpress.org/stats/plugin/1.0/?slug=%s';

    private $fetch_object = [];

    public function __construct($fetch)
    {
      $this->fetch_object = $fetch;
    }

    /* ---
      Functions
    --- */

    public function updateStatsForPlugin($pluginId, $pluginSlug)
    {
      global $wpdb;
      $apiUrl = sprintf(self::API_URL_STATS, $pluginSlug);
      $stats  = $this->fetch_object->getDataFromApi($apiUrl);
      $table  = $wpdb->prefix . Database::TABLE_NAME_STATS;

      $date = current_time('Y-m-d', true);
      foreach ($stats as $version => $percent) {
        $result = $wpdb->get_row("SELECT id FROM {$table} WHERE plugin_id = '{$pluginId}' AND version = '{$version}' AND date = '{$date}'");
        if (!$result) {
          $wpdb->insert($table, [
            'plugin_id' => $pluginId,
            'version'   => $version,
            'percent'   => $percent,
            'date'      => $date,
          ]);
        } else {
          $wpdb->update($table, [
            'percent' => $percent,
          ], [
            'id' => $result->id,
          ]);
        }
      }
    }
  }