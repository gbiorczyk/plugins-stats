<?php

  namespace SiteManagement\Cache;

  class _Core
  {
    public function __construct()
    {
      new Database();
      new Refresh();
    }
  }