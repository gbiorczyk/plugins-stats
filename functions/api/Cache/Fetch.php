<?php

  namespace SiteManagement\Cache;

  class Fetch
  {
    private $cache = [];

    /* ---
      Functions
    --- */

    public function getDataFromApi($url)
    {
      if (!isset($this->cache[$url])) {
        $this->cache[$url] = $this->loadDataFromApi($url);
      }
      return $this->cache[$url];
    }

    private function loadDataFromApi($url)
    {
      $ch  = curl_init($url);
      curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

      $response = json_decode(curl_exec($ch), true);
      $code     = curl_getinfo($ch, CURLINFO_HTTP_CODE);
      curl_close($ch);

      if ($code !== 200) return [];
      else return $response ?? [];
    }
  }