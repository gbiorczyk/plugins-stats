<?php

  namespace SiteManagement\Cache;

  use SiteManagement\Cache\Database;

  class Ratings
  {
    const API_URL_STATS = 'https://api.wordpress.org/plugins/info/1.2/?action=plugin_information&request[slug]=%s';

    private $fetch_object = [];

    public function __construct($fetch)
    {
      $this->fetch_object = $fetch;
    }

    /* ---
      Functions
    --- */

    public function updateStatsForPlugin($pluginId, $pluginSlug)
    {
      global $wpdb;
      $apiUrl = sprintf(self::API_URL_STATS, $pluginSlug);
      $stats  = $this->fetch_object->getDataFromApi($apiUrl);
      $table  = $wpdb->prefix . Database::TABLE_NAME_RATINGS;

      $date   = current_time('Y-m-d', true);
      $result = $wpdb->get_row("SELECT id FROM {$table} WHERE plugin_id = '{$pluginId}' AND date = '{$date}'");
      if (!$result) {
        $wpdb->insert($table, [
          'plugin_id' => $pluginId,
          'stars_1'   => $stats['ratings']['1'],
          'stars_2'   => $stats['ratings']['2'],
          'stars_3'   => $stats['ratings']['3'],
          'stars_4'   => $stats['ratings']['4'],
          'stars_5'   => $stats['ratings']['5'],
          'date'      => $date,
        ]);
      } else {
        $wpdb->update($table, [
          'stars_1'   => $stats['ratings']['1'],
          'stars_2'   => $stats['ratings']['2'],
          'stars_3'   => $stats['ratings']['3'],
          'stars_4'   => $stats['ratings']['4'],
          'stars_5'   => $stats['ratings']['5'],
        ], [
          'id' => $result->id,
        ]);
      }
    }
  }