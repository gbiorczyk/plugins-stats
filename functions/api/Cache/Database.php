<?php

  namespace SiteManagement\Cache;

  class Database
  {
    const OPTION_NAME          = 'stats_sql_tables';
    const TABLE_NAME_INSTALLS  = 'plugins_installs';
    const TABLE_NAME_GROWTH    = 'plugins_growth';
    const TABLE_NAME_RATINGS   = 'plugins_ratings';
    const TABLE_NAME_VERSIONS  = 'plugins_versions';
    const TABLE_NAME_THREADS   = 'plugins_threads';
    const TABLE_NAME_DOWNLOADS = 'plugins_downloads';
    const TABLE_NAME_STATS     = 'plugins_stats';

    public function __construct()
    {
      add_action('after_setup_theme', [$this, 'createTableForDownloads']);
      add_action('after_setup_theme', [$this, 'createTableForGrowth']);
      add_action('after_setup_theme', [$this, 'createTableForInstalls']);
      add_action('after_setup_theme', [$this, 'createTableForRatings']);
      add_action('after_setup_theme', [$this, 'createTableForStats']);
      add_action('after_setup_theme', [$this, 'createTableForThreads']);
      add_action('after_setup_theme', [$this, 'createTableForVersions']);
    }

    /* ---
      Functions
    --- */

    public function createTableForDownloads()
    {
      $sql = 'CREATE TABLE %s (
        id bigint NOT NULL AUTO_INCREMENT,
        plugin_id bigint NOT NULL,
        value int NOT NULL,
        date date NOT NULL,
        PRIMARY KEY (id)
      );';

      $this->createTableInDatabase(self::TABLE_NAME_DOWNLOADS, $sql);
    }

    public function createTableForGrowth()
    {
      $sql = 'CREATE TABLE %s (
        id bigint NOT NULL AUTO_INCREMENT,
        plugin_id bigint NOT NULL,
        value varchar(32) NOT NULL,
        date date NOT NULL,
        PRIMARY KEY (id)
      );';

      $this->createTableInDatabase(self::TABLE_NAME_GROWTH, $sql);
    }

    public function createTableForInstalls()
    {
      $sql = 'CREATE TABLE %s (
        id bigint NOT NULL AUTO_INCREMENT,
        plugin_id bigint NOT NULL,
        value float NOT NULL,
        date date NOT NULL,
        PRIMARY KEY (id)
      );';

      $this->createTableInDatabase(self::TABLE_NAME_INSTALLS, $sql);
    }

    public function createTableForRatings()
    {
      $sql = 'CREATE TABLE %s (
        id bigint NOT NULL AUTO_INCREMENT,
        plugin_id bigint NOT NULL,
        stars_1 int NOT NULL,
        stars_2 int NOT NULL,
        stars_3 int NOT NULL,
        stars_4 int NOT NULL,
        stars_5 int NOT NULL,
        date date NOT NULL,
        PRIMARY KEY (id)
      );';

      $this->createTableInDatabase(self::TABLE_NAME_RATINGS, $sql);
    }

    public function createTableForStats()
    {
      $sql = 'CREATE TABLE %s (
        id bigint NOT NULL AUTO_INCREMENT,
        plugin_id bigint NOT NULL,
        version varchar(32) NOT NULL,
        percent float NOT NULL,
        date date NOT NULL,
        PRIMARY KEY (id)
      );';

      $this->createTableInDatabase(self::TABLE_NAME_STATS, $sql);
    }

    public function createTableForThreads()
    {
      $sql = 'CREATE TABLE %s (
        id bigint NOT NULL AUTO_INCREMENT,
        plugin_id bigint NOT NULL,
        threads int NOT NULL,
        threads_resolved int NOT NULL,
        date date NOT NULL,
        PRIMARY KEY (id)
      );';

      $this->createTableInDatabase(self::TABLE_NAME_THREADS, $sql);
    }

    public function createTableForVersions()
    {
      $sql = 'CREATE TABLE %s (
        id bigint NOT NULL AUTO_INCREMENT,
        plugin_id bigint NOT NULL,
        value varchar(32) NOT NULL,
        date date NOT NULL,
        PRIMARY KEY (id)
      );';

      $this->createTableInDatabase(self::TABLE_NAME_VERSIONS, $sql);
    }

    private function createTableInDatabase($tableSuffix, $sql)
    {
      global $wpdb;

      $table = $wpdb->prefix . $tableSuffix;
      if (($tables = get_option(self::OPTION_NAME, [])) && in_array($table, $tables)) {
        return;
      }

      require_once ABSPATH . 'wp-admin/includes/upgrade.php';
      $sql = sprintf($sql, $table);
      dbDelta($sql);

      $this->updateTablesList($table);
    }

    private function updateTablesList($tableName)
    {
      $tables   = get_option(self::OPTION_NAME, []);
      $tables[] = $tableName;
      update_option(self::OPTION_NAME, $tables);
    }
  }