<?php

  namespace SiteManagement\Cache;

  use SiteManagement\Cache\Database;

  class Refresh
  {
    private $refreshAction = 'stats_cache_refresh';

    public function __construct()
    {
      $this->updateStatsForPlugins();
      $this->initCron();
      add_action($this->refreshAction, [$this, 'updateStatsForPlugins']);
    }

    /* ---
      Functions
    --- */

    private function initCron()
    {
      if (wp_next_scheduled($this->refreshAction)) return;
      wp_schedule_event(time(), 'hourly', $this->refreshAction);
    }

    public function updateStatsForPlugins()
    {
      $plugins = $this->getPlugins();
      $fetch   = new Fetch();

      foreach ($plugins as $plugin) {
        (new Downloads($fetch))->updateStatsForPlugin($plugin->plugin_id, $plugin->plugin_slug);
        (new Growth($fetch))->updateStatsForPlugin($plugin->plugin_id, $plugin->plugin_slug);
        (new Installs($fetch))->updateStatsForPlugin($plugin->plugin_id, $plugin->plugin_slug);
        (new Ratings($fetch))->updateStatsForPlugin($plugin->plugin_id, $plugin->plugin_slug);
        (new Stats($fetch))->updateStatsForPlugin($plugin->plugin_id, $plugin->plugin_slug);
        (new Threads($fetch))->updateStatsForPlugin($plugin->plugin_id, $plugin->plugin_slug);
        (new Versions($fetch))->updateStatsForPlugin($plugin->plugin_id, $plugin->plugin_slug);
      }
    }

    private function getPlugins()
    {
      $plugins = [];
      $postIds = get_posts([
        'post_type' => 'plugins',
        'fields'    => 'ids',
      ]);

      foreach ($postIds as $postId) {
        $plugins[] = (object)[
          'plugin_id'   => $postId,
          'plugin_slug' => get_field('plugin_slug', $postId),
        ];
      }
      return $plugins;
    }
  }