<?php

  namespace SiteManagement\Cache;

  use SiteManagement\Cache\Database;

  class Downloads
  {
    const API_URL_STATS = 'https://api.wordpress.org/stats/plugin/1.0/downloads.php?slug=%s&limit=730';

    private $fetch_object = [];

    public function __construct($fetch)
    {
      $this->fetch_object = $fetch;
    }

    /* ---
      Functions
    --- */

    public function updateStatsForPlugin($pluginId, $pluginSlug)
    {
      global $wpdb;
      $apiUrl = sprintf(self::API_URL_STATS, $pluginSlug);
      $stats  = $this->fetch_object->getDataFromApi($apiUrl);
      $table  = $wpdb->prefix . Database::TABLE_NAME_DOWNLOADS;

      foreach ($stats as $week => $statValue) {
        $result = $wpdb->get_row("SELECT id, value FROM {$table} WHERE plugin_id = '{$pluginId}' AND date = '{$week}'");
        if (!$result) {
          $wpdb->insert($table, [
            'plugin_id' => $pluginId,
            'value'     => $statValue,
            'date'      => $week,
          ]);
        } else if ($result->value != floatval($statValue)) {
          $wpdb->update($table, [
            'value' => $statValue,
          ], [
            'id' => $result->id,
          ]);
        }
      }
    }
  }