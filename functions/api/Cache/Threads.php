<?php

  namespace SiteManagement\Cache;

  use SiteManagement\Cache\Database;

  class Threads
  {
    const API_URL_STATS = 'https://api.wordpress.org/plugins/info/1.2/?action=plugin_information&request[slug]=%s';

    private $fetch_object = [];

    public function __construct($fetch)
    {
      $this->fetch_object = $fetch;
    }

    /* ---
      Functions
    --- */

    public function updateStatsForPlugin($pluginId, $pluginSlug)
    {
      global $wpdb;
      $apiUrl = sprintf(self::API_URL_STATS, $pluginSlug);
      $stats  = $this->fetch_object->getDataFromApi($apiUrl);
      $table  = $wpdb->prefix . Database::TABLE_NAME_THREADS;
      $value  = [
        'support_threads'          => $stats['support_threads'],
        'support_threads_resolved' => $stats['support_threads_resolved'],
      ];

      $date   = current_time('Y-m-d', true);
      $result = $wpdb->get_row("SELECT id FROM {$table} WHERE plugin_id = '{$pluginId}' AND date = '{$date}'");
      if (!$result) {
        $wpdb->insert($table, [
          'plugin_id'        => $pluginId,
          'threads'          => $stats['support_threads'],
          'threads_resolved' => $stats['support_threads_resolved'],
          'date'             => $date,
        ]);
      } else {
        $wpdb->update($table, [
          'threads'          => $stats['support_threads'],
          'threads_resolved' => $stats['support_threads_resolved'],
        ], [
          'id' => $result->id,
        ]);
      }
    }
  }