<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInit48258ba961abd9c3f87137fcb47f7adc
{
    public static $prefixLengthsPsr4 = array (
        'S' => 
        array (
            'SiteManagement\\' => 15,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'SiteManagement\\' => 
        array (
            0 => __DIR__ . '/../..' . '/functions/api',
        ),
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInit48258ba961abd9c3f87137fcb47f7adc::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInit48258ba961abd9c3f87137fcb47f7adc::$prefixDirsPsr4;

        }, null, ClassLoader::class);
    }
}
