<?php

  require_once 'vendor/autoload.php';
  new SiteManagement\SiteManagement();

  if (isset($_GET['load-reviews'])) {

    $plugins = [
      'acf-better-search'        => [1, 4],
      'wp-better-permalinks'     => [2, 1],
      'webp-converter-for-media' => [3, 6],
      'flexible-checkout-fields' => [4, 3],
      'flexible-product-fields'  => [5, 1],
    ];

    foreach ($plugins as $plugin => $pluginData) {
      $content = '';
      for ($i = 1; $i <= $pluginData[1]; $i++) { 
        $content .= file_get_contents('https://wordpress.org/support/plugin/' . $plugin . '/reviews/page/' . $i);
      }
      @$doc    = DOMDocument::loadHTML($content);
      $xpath   = new DOMXPath($doc);
      $ratings = $xpath->query("//li[@class='bbp-body'] //div[@class='wporg-ratings']");
      $dates   = $xpath->query("//li[@class='bbp-body'] //li[@class='bbp-topic-freshness'] /a");

      $list = [];
      foreach ($ratings as $index => $rating) {
        $date  = $dates[$index]->getAttribute('title');
        if ($date === 'September 2, 2020 at 2:47 pm') continue;
        $date  = date('Y-m-d', strtotime(substr($date, 0, (strpos($date, 'at') - 1))));
        $stars = substr($ratings[$index]->getAttribute('title'), 0, 1);
        @$list[$date][$stars]++;
      }
      ksort($list);

      $stars = [
        '5' => 0,
        '4' => 0,
        '3' => 0,
        '2' => 0,
        '1' => 0,
      ];

      global $wpdb;
      $table = $wpdb->prefix . 'plugins_ratings';

      $values = [];
      foreach ($list as $date => $item) {
        foreach ($stars as $index => $star) {
          if (isset($item[$index])) $stars[$index] += $item[$index];
        }
        $wpdb->insert($table, [
          'plugin_id' => $pluginData[0],
          'value'     => json_encode($stars),
          'date'      => $date,
        ]);
        $values[$date] = $stars;
      }
    }

    exit;
  }

  if (isset($_GET['load-versions'])) {

    $plugins = [
      'acf-better-search'        => 1,
      'wp-better-permalinks'     => 2,
      'webp-converter-for-media' => 3,
      'flexible-checkout-fields' => 4,
      'flexible-product-fields'  => 5,
    ];

    foreach ($plugins as $plugin => $pluginData) {
      $content  = file_get_contents('https://wordpress.org/plugins/' . $plugin . '/', false, stream_context_create([
        'ssl' => [
          'verify_peer'      => false,
          'verify_peer_name' => false,
        ],
      ]));
      @$doc     = DOMDocument::loadHTML($content);
      $xpath    = new DOMXPath($doc);
      $versions = $xpath->query("//div[@id='tab-changelog'] /h4");

      $list = [];
      foreach ($versions as $version) {
        preg_match('/([0-9.]+)(?:[–(\s]+)([0-9-]+)/', $version->nodeValue, $matches);
        $list[$matches[2]] = $matches[1];
      }

      global $wpdb;
      $table = $wpdb->prefix . 'plugins_versions';

      $values = [];
      foreach (array_reverse($list) as $date => $item) {
        $wpdb->insert($table, [
          'plugin_id' => $pluginData,
          'value'     => $item,
          'date'      => $date,
        ]);
      }
    }

    exit;
  }